package nom.zh.practice.service;

import java.util.Map;

/**
 * 测试
 */
public interface TestService {

    /**
     * 分页查询测试
     * @return
     * @throws Exception
     */
    Map<String, Object> queryAll() throws Exception;
}
